require_relative 'file_organizer'

print 'Enter source directory: '
source_dir = gets.chomp

print 'Enter destination directory: '
destination_dir = gets.chomp

file_organizer = FileOrganizer.new(source_dir, destination_dir)
file_organizer.organize_files
