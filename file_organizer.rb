require 'fileutils'

class FileOrganizer
  def initialize(source_dir, destination_dir)
    @source_dir = source_dir
    @destination_dir = destination_dir
  end

  def organize_files
    Dir.chdir(@source_dir) do
      Dir.glob('*.*').each do |file|
        extension = File.extname(file)[1..-1]
        destination_folder = File.join(@destination_dir, extension)

        FileUtils.mkdir_p(destination_folder) unless Dir.exist?(destination_folder)
        destination_path = File.join(destination_folder, file)

        FileUtils.mv(file, destination_path)
        puts "Moved #{file} to #{destination_folder}"
      end
    end
  end
end